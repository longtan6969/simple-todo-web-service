import * as express from 'express';
import { routes } from './route/route';

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json())

routes(app)

app.listen(PORT, () => {
     console.log(`Server is running in http://localhost:${PORT}`)
})